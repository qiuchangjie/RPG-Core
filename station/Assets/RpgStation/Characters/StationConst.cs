﻿
namespace Station
{
    
    public class StationConst
    {
        public const string CHARACTER_TYPE = "character_type";
        public const string CHARACTER_ID = "character_id";
        public const string NPC_KEY = "npc_id";
        public const string CLASS_ID = "class_id";
        public const string ICON_ID = "icon";
    }
}

