﻿using UnityEngine;

namespace Station
{
    [CreateAssetMenu]
    public class ItemsSettingsDb : SingleFieldDb<ItemsSettingsModel>
    {

    }
}